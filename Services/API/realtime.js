let sse = require('server-sent-events');

let channels = {};

function getOrCreateChannel(id) {
	if (!channels[id]) {
		channels[id] = {
			queue: [],
			msgId: 0
		};
	}

	return channels[id];
}

function sendMessage(id, key, data) {
	let channel = getOrCreateChannel(id);

	if (channel.response) {
		let id = channel.msgId++;

		let msg = 'id: ' + id + '\n';
		msg = msg + 'event: ' + key + '\n';
		msg = msg + 'data: ' + JSON.stringify(data) + '\n\n';

		channel.response.sse(msg);

		return;
	}

	channel.queue.push({
		key: key,
		data: data
	});
}

function initListener(app, seneca) {
	app.get('/v1/realtime/:id/sse', sse, (req, res) => {
		if (!req.params || !req.params.id) {
			res.sse('connection', { success: false, message: 'ID is required'});
			return;
		}

		var channel = getOrCreateChannel(req.params.id);

		channel.response = res;

		sendMessage(req.params.id, 'connection', { success: true });

		channel.queue.forEach(msg => {
			sendMessage(req.params.id, msg.key, msg.data);
		});
	});

	seneca.add('role:api-realtime,cmd:send', (msg, respond) => {
		sendMessage(msg.id, msg.key, msg.data);
		respond();
	});
}

function init(opts) {
	this.add('role:api-realtime,cmd:rooms/:id/send', (msg, respond) => {
		let args = msg.args;
		args.cookies = msg.request$.cookies;
		args.sessionId = args.cookies && args.cookies['session_id'];

		this.act('role:realtime,cmd:roomsend', msg.args, respond);
	});
}

let routes = [{
	prefix: '/v1/realtime',
	pin: 'role:api-realtime,cmd:*',
	map: {
		'rooms/:id/send': {
			POST: true
		},
	}
}];

module.exports = {
	initListener: initListener,
	init: init,
	routes: routes
};