module.exports = {
	routes: [{
		prefix: '/v1/directory',
		pin: 'role:api-directory,cmd:*',
		map: {
			'rooms/:query': {
				GET: true,
			},
			'rooms/:id/join': {
				POST: true
			},
			'rooms': {
				GET: true,
			},
			'create': {
				POST: true
			}
		}
	}],
	init: function(opts) {
		this.add('role:api-directory,cmd:rooms/:query', (msg, respond) => {
			this.act('role:directory,cmd:list', msg.args, respond);
		});

		this.add('role:api-directory,cmd:rooms', (msg, respond) => {
			this.act('role:directory,cmd:list', msg.args, respond);
		});

		this.add('role:api-directory,cmd:create', (msg, respond) => {
			this.act('role:directory,cmd:create', msg.args, respond);
		});

		this.add('role:api-directory,cmd:rooms/:id/join', (msg, respond) => {
			console.log(msg);

			let args = msg.args;
			args.cookies = msg.request$.cookies;
			args.sessionId = args.cookies && args.cookies['session_id'];

			this.act('role:directory,cmd:join', msg.args, respond);
		});
	},
};