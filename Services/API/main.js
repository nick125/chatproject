let _ = require('lodash');

let express = require('express');
let Seneca = require('seneca');
let senecaWeb = require('seneca-web');

let app = express();
app.use(require('body-parser').json());
app.use(require('cookie-parser')());

let realtimeApi = require('./realtime');

let seneca = Seneca();

// Realtime is a special case, so it doesn't register a route through seneca.
realtimeApi.initListener(app, seneca);

let directoryApi = require('./directory');
let authApi = require('./auth');

let config = _.concat([], directoryApi.routes, authApi.routes, realtimeApi.routes);

seneca.use(senecaWeb, { adapter: 'express', context: app });
seneca.use(directoryApi.init);
seneca.use(authApi.init);
seneca.use(realtimeApi.init);

seneca.client({ type: 'tcp', pin: 'role:directory', host: 'localhost'});
seneca.client({ type: 'tcp', pin: 'role:auth', host: 'localhost', port: 10202 });
seneca.client({ type: 'tcp', pin: 'role:realtime', host: 'localhost', port: 10203 });
seneca.listen({ type: 'tcp', host: 'localhost', port: 10204 });

seneca.act('role:web', {options: { parseBody: false}, routes: config}, (err, reply) => {
  console.log(err || reply.routes)
});

seneca.ready(() => {
	app.listen(3000);
});