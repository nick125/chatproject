module.exports = {
	routes: [{
		prefix: '/v1/auth',
		pin: 'role:api-auth,cmd:*',
		map: {
			'connection': {
				POST: true,
			},
			'connection/:id': {
				DELETE: true
			}
		}
	}],
	init: function(opts) {
		this.add('role:api-auth,cmd:connection', (msg, respond) => {
			this.act('role:auth,cmd:connect', msg, (obj, resp) => {
				if (resp.success && resp.sessionId) {
					msg.response$.cookie('session_id', resp.sessionId);
				} 

				respond(obj, resp);
			});
		});

		this.add('role:api-auth,cmd:connection/:id', (msg, respond) => {
			this.act('role:auth,cmd:abandon', msg, (obj, resp) => {
				if (resp.success) {
					msg.response$.clearCookie('session_id');
				}

				respond(obj, resp);
			});
		})
	},
};