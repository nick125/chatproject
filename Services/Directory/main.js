let Seneca = require('seneca');
let seneca = Seneca();

seneca.use('directory');
seneca.client({ type: 'tcp', pin: 'role:realtime', host: 'localhost', port: 10203 });
seneca.client({ type: 'tcp', pin: 'role:api-realtime', host: 'localhost', port: 10204 });
seneca.listen({ type: 'tcp', host: 'localhost' });