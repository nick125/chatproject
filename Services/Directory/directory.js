let _ = require('lodash');
let AWS = require('aws-sdk');
let Guid = require('guid');
let redis = require('redis');

AWS.config.update({
	region: 'us-west-2',
	endpoint: 'http://localhost:8000'
});

module.exports = function(opts) {
	let docClient = new AWS.DynamoDB.DocumentClient();
	let rClient = redis.createClient();

	this.add('role:directory,cmd:join', (msg, respond) => {
		let room = msg && msg && msg.params && msg.params.id || '';
		let sessionId = msg && msg.sessionId;

		if (_.isEmpty(room)) {
			respond(null, {success: false, message: "No room specified"});
			return;
		}

		if (_.isEmpty(sessionId)) {
			respond(null, {success: false, message: "No session for the user"});
			return;
		}

		// Check that the room exists
		let roomQuery = {
			TableName: 'rooms',
			Key: {
				id: room
			}
		};

		docClient.get(roomQuery, (err, roomData) => {
			if (err) {
				respond(null, {success: false, message: "Room does not exist"});
				return;
			}

			// If that was successful, then update the room roster
			rClient.sadd('rooms:' + room + '--roster', sessionId, (err) => {
				if (err) {
					respond(null, {success: false, message: "Could not update chat roster"});
					return;
				}

				// Get the new list of users
				rClient.smembers('rooms:' + room + '--roster', (err, rosterMembers) => {
					if (err) {
						respond(null, {success: false, message: "Could not get the chat roster"});
						return;
					}

					// Notify existing users 
					rosterMembers.forEach(memberId => {
						this.act('role:api-realtime,cmd:send', {
							id: memberId,
							key: 'join',
							data: {
								user: sessionId,
							}
						});
					});

					// Send the response
					respond(null, { success: true, members: rosterMembers, title: roomData.title, topic: roomData.topic });
				});
			});
		});
	});

	this.add('role:directory,cmd:list', (msg, respond) => {
		let query = msg && msg.body && msg.body.params && msg.body.params.query || '*';

		docClient.scan({ TableName: 'rooms'}, (err, data) => {
			if (err) {
				console.log(err);
				respond(null, {success: false});
				return;
			}

			console.log(data);

			respond(null, {
				'rooms': _.map(data.Items, (v, k) => {
					// // Query the roster count
					// rClient.scard('rooms:' + v.id + '--roster', (err, count) => {
					// 	if (err) {
					// 		respond(null, {success: false, message: "Could not get the chat roster"});
					// 		return;
					// 	}

					// 	// Send the response
					// 	respond(null, { success: true, members: count, title: v.title, topic: v.topic });
					// });
					return v
				})
			});
		})
	});

	this.add('role:directory,cmd:create', (msg, respond) => {
		let id = Guid.raw();
		let directoryDefinition = {
			id: id, 
			title: msg.body.title || '',
			topic: msg.body.topic || ''
		};

		var insertParams = {
			TableName: 'rooms',
			Item: directoryDefinition,
			ConditionalExpression: 'attribute_not_exists(id)'
		}

		console.log(insertParams);

		docClient.put(insertParams, (err, data) => {
			if (err) {
				console.log(err);
				respond(null, {success: false});
				return;
			}

			respond(null, {success: true, id: id});
		});
	});
};