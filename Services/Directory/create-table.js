let AWS = require('aws-sdk');

AWS.config.update({
	region: 'us-west-2',
	endpoint: 'http://localhost:8000'
});

let dynamoDb = new AWS.DynamoDB();


let roomsTable = {
	TableName: 'rooms',
	KeySchema: [
		{ AttributeName: 'id', KeyType: 'HASH' }
	],
	AttributeDefinitions: [
		{ AttributeName: 'id', AttributeType: 'S' }
	],
	ProvisionedThroughput: {
		ReadCapacityUnits: 1,
		WriteCapacityUnits: 1
	}
}

dynamoDb.createTable(roomsTable, (err, data) => {
	if (err) {
		console.log(err);
	} else {
		console.log(data);
	}
});