let _ = require('lodash');
let Guid = require('guid');
let AWS = require('aws-sdk');
let redis = require('redis');

AWS.config.update({
	region: 'us-west-2',
	endpoint: 'http://localhost:8000'
});

module.exports = function(opts) {
	let rClient = redis.createClient();

	this.add('role:auth,cmd:connect', (msg, respond) => {
		let name = msg && msg.args && msg.args.body && msg.args.body.name || '';
		let id = Guid.raw();

		if (_.isEmpty(name)) {
			respond(null, { success: false, message: 'Must specify a name' });
			return;
		}

		rClient.hmset('user:' + id, {
			active: true
		}, (err) => {
			if (err) {
				respond(null, { success: false });
				return;
			}

			respond(null, { success: true, sessionId: id });
		});
	});

	this.add('role:auth,cmd:abandon', (msg, respond) => {
		let id = msg && msg.args && msg.args.params && msg.args.params.id || '';

		if (_.isEmpty(id)) {
			respond(null, { success: false, message: 'Must specify an id' });
			return;
		}

		console.log('Deleting session ' + id);

		rClient.del('user:' + id, (err) => {
			console.log('got Redis resp');
			
			if (err) {
				respond(null, { success: false });
				return;
			}

			respond(null, { success: true });
		});
	});
};