let Seneca = require('seneca');
let seneca = Seneca();

seneca.use('auth');
seneca.client({ type: 'tcp', pin: 'role:realtime', host: 'localhost', port: 10203 });
seneca.listen({ type: 'tcp', host: 'localhost', port: 10202 });