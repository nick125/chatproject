let _ = require('lodash');
let redis = require('redis');

module.exports = function(opts) {
	let rClient = redis.createClient();

	this.add('role:realtime,cmd:roomsend', (msg, respond) => {
		let userId = msg && msg.sessionId;
		let roomId = msg && msg && msg.params && msg.params.id || '';
		let message = msg && msg.body && msg.body.message || '';
		console.log(msg);

		if (!userId || !roomId) {
			respond(null, {success: false, message:'Parameter missing'});
			return;
		} 

		// Verify that the user is in the room roster
		console.log('Checking if user ' + userId + ' is a member of ' + roomId);
		rClient.sismember('rooms:' + roomId + '--roster', userId, (err, res) => {
			if (err || res <= 0) {
				respond(null, {success: false, message: 'Could not determine user membership'});
				return;
			}

			// Get the members
			rClient.smembers('rooms:' + roomId + '--roster', (err, res) => {
				if (err || res <= 0) {
					respond(null, {success: false, message: 'Could not determine user membership'});
					return;
				}
				
				// Broadcast the message to each user
				res.forEach(memberId => {
					this.act('role:api-realtime,cmd:send', {
						id: memberId,
						key: 'message',
						data: {
							user: userId,
							room: roomId,
							message: message
						}
					});
				});

				respond(null, {success: true});
			});
		});
	});
}