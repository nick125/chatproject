let EventSource = require('EventSource');
let es = new EventSource(process.argv[2]);

process.argv.slice(2).forEach(k => {
	es.addEventListener(k, (e) => {
		console.log(' ----- GOT EVENT ' + k + ' -----')
		console.log(e);
	});
});

es.onmessage = (event) => {
	console.log('------------ EVENT ---------------');
	console.log(event);
}

es.onopen = () => {
	console.log('Opened connection');
}

es.onerror = (err) => {
	console.log('ERROR');
	console.log(err);
}