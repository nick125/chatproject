let Seneca = require('seneca');
let seneca = Seneca();

seneca.use('realtime');
seneca.client({ type: 'tcp', pin: 'role:directory', host: 'localhost'});
seneca.client({ type: 'tcp', pin: 'role:api-realtime', host: 'localhost', port: 10204 });
seneca.listen({ type: 'tcp', host: 'localhost', port: 10203 });